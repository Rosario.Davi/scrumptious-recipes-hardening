# here we imported all our views, imported a login_required,
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView
from django.urls import reverse_lazy
from django.shortcuts import redirect

from .models import Mealplan


class MealplanListView(LoginRequiredMixin, ListView):
    # wrtie the model
    model = Mealplan
    # write template name creating the html for path
    template_name = "meal_plans/list.html"
    # create the function to query a requested user
    context_object_name = "meals"

    def get_queryset(self):
        return Mealplan.objects.filter(owner=self.request.user)


class MealplanCreateView(LoginRequiredMixin, CreateView):
    # create view displays a form for creating an object
    model = Mealplan
    template_name = "meal_plans/new.html"
    # field = [list of fields for user to creare]
    # field view is showing a form that allows the user to enter a name a date
    # and selects the recipes

    # create the function for valid form
    def form_valid(self, form):
        plan = form.save(commit=False)
        plan.owner = self.request.user
        plan.save()
        form.save_m2m()
        return redirect("meal_plans", pk=plan.id)


class MealplanDetailView(LoginRequiredMixin, DetailView):
    model = Mealplan
    template_name = "meal_plans/detail.html"

    def get_queryset(self):
        return Mealplan.objects.filter(owner=self.request.user)

    #
    #


class MealplanUpdateView(LoginRequiredMixin, UpdateView):
    model = Mealplan
    template_name = "meal_plans/edit.html"
    # fields = [list of fields for user to create]

    def get_queryset(self):
        return Mealplan.objects.filter(owner=self.request.user)

    def get_success_url(self) -> str:
        return reverse_lazy("meal_plans_detail", args=[self.object.id])


class MealplanDeleteView(LoginRequiredMixin, DeleteView):
    model = Mealplan
    template_name = "meal_plans/delete.html"
    success_url = reverse_lazy("meal_plans_lists")

    def get_queryset(self):
        return Mealplan.objects.filter(owner=self.request.user)
