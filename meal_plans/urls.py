from django.urls import path
from .views import (
    MealplanCreateView,
    MealplanDeleteView,
    MealplanDetailView,
    MealplanListView,
    MealplanUpdateView,
)


urlpatterns = [
    path("", MealplanListView.as_view(), name="meal_plans"),
    path(
        "meal_plans/create/", MealplanCreateView.as_view(), name="meal_create"
    ),
    path(
        "meal_plans/<int:pk>/", MealplanDetailView.as_view(), name="meal_detail"
    ),
    path(
        "meal_plans/<int:pk>/edit/",
        MealplanUpdateView.as_view(),
        name="meal_edit",
    ),
    path(
        "meal_plans/<int:pk>/delete/",
        MealplanDeleteView.as_view(),
        name="meal_delete",
    ),
]
"""
meal_plans/	Show a list of meal plans created by the user
meal_plans/create/	Show a form that allows a user to create a meal plan
meal_plans/<int:pk>/	Show the details of one of the user's meal plans
meal_plans/<int:pk>/edit/	Show an edit form to edit one of their meal plans
meal_plans/<int:pk>/delete/	Show a form that allows a user to delete one of their meal plans
"""
