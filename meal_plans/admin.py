from django.contrib import admin

from .models import Mealplan

# Register your models here.
admin.site.register(Mealplan)
